import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:epub_viewer/epub_viewer.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool loading = false;
  Dio dio = new Dio();


  final imgUrl = "http://bbebooksthailand.com/phpscripts/bbdownload.php?ebookdownload=CrimePunishment-EPUB2";
  bool downloading = false;
  var progressString = "";


  @override
  void initState() {
    super.initState();
    download();
  }

  download() async {
    if (Platform.isAndroid) {
      print('download');
      await downloadFile();
    } else {
      loading = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: downloading
              ? Container(
            height: 120.0,
            width: 200.0,
            child: Card(
              color: Colors.black,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Downloading File: $progressString",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
          )

              : FlatButton(
            onPressed: () async {
              Directory appDocDir =
              await getApplicationDocumentsDirectory();
              print('$appDocDir');

              // String iosBookPath = '${appDocDir.path}/chair.epub';
              // print(iosBookPath);
              // String androidBookPath = 'file:///android_asset/3.epub';
              EpubViewer.setConfig(
                  themeColor: Theme.of(context).primaryColor,
                  identifier: "androidBook",
                  scrollDirection: EpubScrollDirection.ALLDIRECTIONS,
                  allowSharing: true,
                  enableTts: true,
                  nightMode: true);

              String androidBookPath = 'data/user/0/com.example.epub_flutter/app_flutter/book.epub';

                   EpubViewer.open(

                     Platform.isAndroid ? androidBookPath : androidBookPath,
                     lastLocation: EpubLocator.fromJson({"bookId":"urn:uuid:12c6fed8-ec29-4343-ab36-9a48312ee01d","href":"/OEBPS/c/c13.html","created":1640076755424,"locations":{"cfi": "epubcfi(/0!/4/46/1:0)"},"title":""}),

                   );

              // await EpubViewer.openAsset(
              //   '/data/user/0/com.example.epub_flutter/app_flutter/book.epub',
              //   lastLocation: EpubLocator.fromJson({
              //     "bookId":"urn:uuid:12c6fed8-ec29-4343-ab36-9a48312ee01d","href":"/OEBPS/coverpage.html","created":1639554909535,"locations":{"cfi":"epubcfi(/0!/4/2[coverimage_id]/2)"},"title":""}
              //
              //   ),
              // );
              // get current locator


              // EpubViewer.locatorStream.listen((locator) {
              //   print(
              //       'LOCATOR: ${locator.toString()}');
              // });

              EpubViewer.locatorStream.listen((event) async {
                // Get locator here
                Map json = jsonDecode(event);
                // json['bookId'] = widget.entry.id.t.toString();
                // // Save locator to your database
                // await LocatorDB().update(json);
                print('LOCATION: ${json['bookId']}');
                print('LOCATION2: ${json['href']}');
                print('LOCATION3: ${json['created']}');
                print('LOCATION4: ${json['locations']}');

              });


            },
            child: Container(
              child: Text('open epub'),
            ),
          ),
        ),
      ),
    );
  }

  // Future downloadFile() async {
  //   print('download1');
  //
  //   if (await Permission.storage.isGranted) {
  //     await Permission.storage.request();
  //     await startDownload();
  //   } else {
  //     await startDownload();
  //   }
  // }


  Future<void> downloadFile() async {
    Dio dio = Dio();

    try {
      var dir = await getApplicationDocumentsDirectory();
      print("path ${dir.path}");
      await dio.download(imgUrl, "${dir.path}/book.epub",
          onReceiveProgress: (rec, total) {
            print("Rec: $rec , Total: $total");

            setState(() {
              downloading = true;
              progressString = ((rec / total) * 100).toStringAsFixed(0) + "%";
            });
          });
    } catch (e) {
      print(e);
    }

    setState(() {
      downloading = false;
      progressString = "Completed";
    });
    print("Download completed");
  }




  startDownload() async {
    Directory appDocDir = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();

    String path = appDocDir.path + '/chair.epub';
    File file = File(path);
//    await file.delete();

    if (!File(path).existsSync()) {
      await file.create();
      await dio.download(
        'https://github.com/FolioReader/FolioReaderKit/raw/master/Example/'
            'Shared/Sample%20eBooks/The%20Silver%20Chair.epub',
        path,
        deleteOnError: true,
        onReceiveProgress: (receivedBytes, totalBytes) {
          print((receivedBytes / totalBytes * 100).toStringAsFixed(0));
          //Check if download is complete and close the alert dialog
          if (receivedBytes == totalBytes) {
            loading = false;
            setState(() {});
          }
        },
      );
    } else {
      loading = false;
      setState(() {});
    }
  }
}